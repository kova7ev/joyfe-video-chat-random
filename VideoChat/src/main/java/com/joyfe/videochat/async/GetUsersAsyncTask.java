package com.joyfe.videochat.async;

import android.os.AsyncTask;
import android.util.Log;

import com.quickblox.core.QBCallback;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.module.users.QBUsers;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.users.result.QBUserPagedResult;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by root on 06/04/14.
 */
public class GetUsersAsyncTask extends AsyncTask<QBCallbackImpl, Void, Void> {
    private QBCallbackImpl callback;

    @Override
    protected Void doInBackground(QBCallbackImpl... callbacks) {
        callback = callbacks[0];
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        QBUsers.getUsers(callback);
    }
}
