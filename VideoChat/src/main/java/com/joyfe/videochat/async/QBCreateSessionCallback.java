package com.joyfe.videochat.async;

import com.joyfe.videochat.model.utils.CustomObjectHelper;
import com.joyfe.videochat.model.utils.UserHelper;
import com.quickblox.core.QBCallback;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.result.Result;
import com.quickblox.module.auth.QBAuth;
import com.quickblox.module.auth.result.QBSessionResult;
import com.quickblox.module.chat.QBChatService;
import com.quickblox.module.chat.listeners.SessionListener;
import com.quickblox.module.users.QBUsers;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.QBVideoChatController;

import org.jivesoftware.smack.XMPPException;

/**
 * Created by root on 09/04/14.
 */
public class QBCreateSessionCallback implements QBCallback {
    private QBUser mUser;
    private QBCallback mCallback;

    public QBCreateSessionCallback(QBUser user, QBCallback callback){
        mUser = user;
        mCallback = callback;
    }

    @Override
    public void onComplete(Result result) {
        if (result.isSuccess()) {
            mUser.setId(((QBSessionResult) result).getSession().getUserId());

            QBUsers.signIn(mUser, new QBCallbackImpl() {
                @Override
                public void onComplete(Result result) {
                    if (result.isSuccess()) {
                        CustomObjectHelper.updateCustomObject(mUser, true, false);
                        UserHelper.saveCurrentUser(mUser);
                        QBChatService.getInstance().loginWithUser(mUser, loginListener);
                    }
                }
            });
        } else {
            createUser(mUser, new QBCallbackImpl() {

                @Override
                public void onComplete(Result result) {
                    if (result.isSuccess()) {
                        QBAuth.createSession(mUser.getLogin(), mUser.getPassword(), this);
                    }
                }
            });
        }
    }

    @Override
    public void onComplete(Result result, Object context) {

    }

    private void createUser(final QBUser user, final QBCallback callback) throws RuntimeException {
        QBAuth.createSession(new QBCallbackImpl() {
            @Override
            public void onComplete(Result result) {
                if (result.isSuccess()) {
                    QBUsers.signUp(user, callback);
                }
            }
        });
    }

    private SessionListener loginListener = new SessionListener() {
        @Override
        public void onLoginSuccess() {

            try {
                QBVideoChatController.getInstance().initQBVideoChatMessageListener();
            } catch (XMPPException e) {
                e.printStackTrace();
            }

            mCallback.onComplete(null);

        }

        @Override
        public void onLoginError() {
        }

        @Override
        public void onDisconnect() {
        }

        @Override
        public void onDisconnectOnError(Exception exc) {
        }
    };
}