package com.joyfe.videochat.async;

import android.os.AsyncTask;

import com.joyfe.videochat.model.utils.CustomObjectHelper;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.module.users.model.QBUser;

/**
 * Created by root on 06/04/14.
 */
public class SendLivePacketAsyncTask extends AsyncTask<QBCallbackImpl, Void, Void> {
    private QBCallbackImpl mCallback;
    private QBUser mUser;

    public SendLivePacketAsyncTask(QBUser user){
        mUser = user;
    }

    @Override
    protected Void doInBackground(QBCallbackImpl... callbacks) {
        mCallback = callbacks[0];
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        CustomObjectHelper.sendLivePacket(mUser, mCallback);
        super.onPostExecute(aVoid);

    }
}