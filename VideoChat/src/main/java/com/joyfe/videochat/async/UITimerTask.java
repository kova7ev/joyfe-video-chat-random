package com.joyfe.videochat.async;

import android.os.Handler;
import android.os.Looper;

import java.util.TimerTask;

/**
 * Created by root on 10/04/14.
 */
public class UITimerTask extends TimerTask {
    protected Handler mHandler;

    public UITimerTask() {
        Looper looper = Looper.getMainLooper();
        //looper.prepareMainLooper();
        mHandler = new Handler(looper);
    }

    @Override
    public void run() {
        throw new RuntimeException();
    }
}