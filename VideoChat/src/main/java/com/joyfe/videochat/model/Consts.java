package com.joyfe.videochat.model;

/**
 * Created by root on 07/04/14.
 */
public interface Consts {
    public static final String CLASS_NAME = "UserState";
    public static final String USER_ID = "userId";
    public static final String ONLINE = "online";
    public static final String BUSY = "busy";
    public static final String LAST_LIVE_PACKET_AT = "lastLivePacketAt";
}
