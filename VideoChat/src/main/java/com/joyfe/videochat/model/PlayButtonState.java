package com.joyfe.videochat.model;

/**
 * Created by root on 08/04/14.
 */
public enum PlayButtonState {
    Play,
    Pause,
    Stop
}