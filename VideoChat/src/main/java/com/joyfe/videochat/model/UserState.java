package com.joyfe.videochat.model;

import com.quickblox.module.custom.model.QBCustomObject;
import com.quickblox.module.users.model.QBUser;

/**
 * Created by root on 07/04/14.
 */
public class UserState {
    private int mUserId;
    private boolean mOnline;
    private boolean mBusy;
    private long mLastLivePacketAt;

    public UserState(QBCustomObject obj){
        mUserId = Integer.parseInt(obj.getFields().get(Consts.USER_ID).toString());
        mOnline = Boolean.parseBoolean (obj.getFields().get(Consts.ONLINE).toString());
        mBusy = Boolean.parseBoolean(obj.getFields().get(Consts.BUSY).toString());
        mLastLivePacketAt = Long.parseLong(obj.getFields().get(Consts.LAST_LIVE_PACKET_AT).toString());
    }

    public UserState(QBUser user, boolean online, boolean busy){
        mUserId = user.getId();
        mOnline = online;
        mBusy = busy;
    }

    public int getUserId(){
        return mUserId;
    }

    public boolean isOnline(){
        return mOnline;
    }

    public boolean isBusy(){
        return mBusy;
    }

    public long getLastLivePacketAt(){
        return mLastLivePacketAt;
    }
}
