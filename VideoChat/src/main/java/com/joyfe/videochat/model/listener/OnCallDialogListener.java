package com.joyfe.videochat.model.listener;

public interface OnCallDialogListener {

    public void onAcceptCallClick();
    public void onRejectCallClick();
}
