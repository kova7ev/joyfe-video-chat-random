package com.joyfe.videochat.model.utils;

import com.joyfe.videochat.model.Consts;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.result.Result;
import com.quickblox.internal.module.custom.request.QBCustomObjectRequestBuilder;
import com.quickblox.module.custom.QBCustomObjects;
import com.quickblox.module.custom.model.QBCustomObject;
import com.quickblox.module.custom.result.QBCustomObjectLimitedResult;
import com.quickblox.module.users.model.QBUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 07/04/14.
 */
public class CustomObjectHelper {
    public static void addCustomObject(QBUser user, boolean online, boolean busy, QBCallbackImpl impl) {
        HashMap<String, Object> fields = new HashMap<String, Object>();

        fields.put(Consts.USER_ID, user.getId());
        fields.put(Consts.ONLINE, online);
        fields.put(Consts.BUSY, busy);
        fields.put(Consts.LAST_LIVE_PACKET_AT, Calendar.getInstance().getTimeInMillis());

        QBCustomObject qbCustomObject = new QBCustomObject();
        qbCustomObject.setClassName(Consts.CLASS_NAME);
        qbCustomObject.setFields(fields);
        QBCustomObjects.createObject(qbCustomObject, impl);
    }

    public static void Live(QBUser user, boolean online, boolean busy) {
        addCustomObject(user, online, busy, new QBCallbackImpl() {
            @Override
            public void onComplete(com.quickblox.core.result.Result result) {
                if (result.isSuccess()) {
                    //TODO: unimplemented
                }
            }
        });
    }

    public static void addCustomObject(QBUser user, boolean online, boolean busy) {
        addCustomObject(user, online, busy, new QBCallbackImpl() {
            @Override
            public void onComplete(com.quickblox.core.result.Result result) {
                if (result.isSuccess()) {
                    //TODO: unimplemented
                }
            }
        });
    }

    public static void sendLivePacket(final QBUser user, final QBCallbackImpl impl) {
        QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
        requestBuilder.eq(Consts.USER_ID, user.getId());

        QBCustomObjects.getObjects(Consts.CLASS_NAME, requestBuilder, new QBCallbackImpl() {
            @Override
            public void onComplete(Result result) {
                if (result.isSuccess()) {
                    ArrayList<QBCustomObject> objects = ((QBCustomObjectLimitedResult) result).getCustomObjects();

                    if (objects.size() == 0) {
                        addCustomObject(user, true, false, impl);
                    }
                    else{
                        QBCustomObject object = objects.get(0);

                        HashMap<String, Object> fields = new HashMap<String, Object>();

                        fields.put(Consts.USER_ID, user.getId());
                        fields.put(Consts.ONLINE, true);
                        fields.put(Consts.BUSY, false);
                        fields.put(Consts.LAST_LIVE_PACKET_AT, Calendar.getInstance().getTimeInMillis());

                        object.setFields(fields);
                        QBCustomObjects.updateObject(object, impl);
                    }
                }
            }
        });
    }

    public static void sendLivePacket(QBUser user) {
        sendLivePacket(user, new QBCallbackImpl());
    }

    public static void updateCustomObject(final QBUser user, final boolean online, final boolean busy, final QBCallbackImpl impl) {
        QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
        requestBuilder.eq(Consts.USER_ID, user.getId());

        QBCustomObjects.getObjects(Consts.CLASS_NAME, requestBuilder, new QBCallbackImpl() {
            @Override
            public void onComplete(Result result) {
                if (result.isSuccess()) {
                    ArrayList<QBCustomObject> objects = ((QBCustomObjectLimitedResult) result).getCustomObjects();

                    if (objects.size() == 0) {
                        addCustomObject(user, online, busy, impl);
                    }
                    else{
                        QBCustomObject object = objects.get(0);

                        HashMap<String, Object> fields = new HashMap<String, Object>();

                        fields.put(Consts.USER_ID, user.getId());
                        fields.put(Consts.ONLINE, online);
                        fields.put(Consts.BUSY, busy);

                        object.setFields(fields);
                        QBCustomObjects.updateObject(object, impl);
                    }
                }
            }
        });
    }

    public static void updateCustomObject(final QBUser user, final boolean online, final boolean busy) {
        updateCustomObject(user, online, busy, new QBCallbackImpl() {
            @Override
            public void onComplete(com.quickblox.core.result.Result result) {
                //TODO: unimplemented
            }
        });
    }

    public static void getCustomObjects(QBCallbackImpl impl) {
        QBCustomObjects.getObjects(Consts.CLASS_NAME, impl);
    }

    public static void getCustomObjects() {
        getCustomObjects(new QBCallbackImpl() {
            @Override
            public void onComplete(com.quickblox.core.result.Result result) {
                List<QBCustomObject> qbCustomObjects = ((QBCustomObjectLimitedResult) result).getCustomObjects();
            }

            @Override
            public void onComplete(Result result, Object context) {
                List<QBCustomObject> qbCustomObjects = ((QBCustomObjectLimitedResult) result).getCustomObjects();
            }
        });
    }
}