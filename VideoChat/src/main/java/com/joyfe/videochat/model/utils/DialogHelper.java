package com.joyfe.videochat.model.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.View;

import com.joyfe.videochat.R;
import com.joyfe.videochat.model.listener.OnCallDialogListener;
import com.quickblox.module.videochat.model.definition.VideoChatConstants;

public class DialogHelper {

    private static AlertDialog.Builder builder;

    public static AlertDialog showCallDialog(Context context, final OnCallDialogListener callDialogListener, final DialogInterface.OnCancelListener onCancelListener) {
        if (builder == null) {
            DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            callDialogListener.onAcceptCallClick();
                            deleteCallDialog();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            callDialogListener.onRejectCallClick();
                            deleteCallDialog();
                            break;
                    }
//                    dismissDialog();
                }
            };
            builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(R.string.calling_dialog_title))
                    .setMessage(R.string.calling_dialog_txt)
                    .setPositiveButton(VideoChatConstants.YES, onClickListener)
                    .setNegativeButton(VideoChatConstants.NO, onClickListener)
                    .setOnCancelListener(onCancelListener)
                    .show();
        }

        return builder.create();
    }

    public static AlertDialog showNextUserDialog(Context context, final OnCallDialogListener callDialogListener, final Dialog.OnCancelListener onCancelListener) {
        if (builder == null) {
            DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            callDialogListener.onAcceptCallClick();
                            deleteCallDialog();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            callDialogListener.onRejectCallClick();
                            deleteCallDialog();
                            break;
                    }
//                    dismissDialog();
                }
            };
            builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(R.string.next_dialog_title))
                    .setMessage(R.string.next_dialog_txt)
                    .setPositiveButton(VideoChatConstants.YES, onClickListener)
                    .setNegativeButton(VideoChatConstants.NO, onClickListener)
                    .setOnCancelListener(onCancelListener)
                    .show();
        }

        return builder.create();
    }


    private static void deleteCallDialog() {
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                builder = null;
            }
        }, 2000);
    }

    public static void dismissDialog() {
        builder = null;
    }
}