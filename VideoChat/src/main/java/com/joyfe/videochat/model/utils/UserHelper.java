package com.joyfe.videochat.model.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;

import com.joyfe.videochat.model.UserState;
import com.quickblox.core.QBCallback;
import com.quickblox.module.users.QBUsers;
import com.quickblox.module.users.model.QBUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by root on 09/04/14.
 */
public class UserHelper {
    private static Context mContext;
    private static QBUser mUser;

    public static void init(Context context){
        mContext = context;
    }

    public static void saveCurrentUser(QBUser user){
        mUser = user;
    }

    public static QBUser getCurrentUser(){
        return mUser;
    }

    public static String getUsername() {
        if (mContext == null){
            throw new RuntimeException("Please first call 'UserHelper.init(Context context)'");
        }

        String username = getUsernameFromEmail();

        if (username == null) {
            username = generateRandomName();
        }

        username = String.format("%s_%s", username, android.os.Build.MODEL.replaceAll("[^a-zA-Z0-9]", ""));
        return username;
    }

    public static void getUsers(QBCallback callback) {
        QBUsers.getUsers(callback);
    }

    public static boolean isOnline(QBUser user, UserState userState) {
        return userState.isOnline() && (Calendar.getInstance().getTimeInMillis() - userState.getLastLivePacketAt()) < 10 * 1000;
    }

    public static boolean isIgnored(QBUser user, ArrayList<QBUser> users) {
        for (int i = 0; i < users.size(); i++) {
            if (user.getId().equals(users.get(i).getId())) {
                return true;
            }
        }

        return false;
    }

    public static boolean isCurrentUser(QBUser user) {
        return mUser.getId().equals(user.getId());
    }

    private static String generateRandomName() {
        return String.format("User%d", (int) (Math.random() * 1000));
    }

    private static String getUsernameFromEmail() {
        AccountManager manager = AccountManager.get(mContext);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (Account account : accounts) {
            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");
            if (parts.length > 0 && parts[0] != null)
                return parts[0];
            else
                return null;
        } else
            return null;
    }
}
