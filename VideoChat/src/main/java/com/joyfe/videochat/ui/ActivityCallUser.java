package com.joyfe.videochat.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.joyfe.videochat.R;
import com.joyfe.videochat.model.listener.OnCallDialogListener;
import com.joyfe.videochat.model.utils.DialogHelper;
import com.joyfe.videochat.model.utils.UserHelper;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.videochat.core.QBVideoChatController;
import com.quickblox.module.videochat.model.listeners.OnQBVideoChatListener;
import com.quickblox.module.videochat.model.objects.CallState;
import com.quickblox.module.videochat.model.objects.CallType;
import com.quickblox.module.videochat.model.objects.VideoChatConfig;
import com.quickblox.module.videochat.model.utils.Debugger;

import org.jivesoftware.smack.XMPPException;

public class ActivityCallUser extends Activity {

    private Handler autoCancelHandler = new Handler(Looper.getMainLooper());
    private ProgressDialog progressDialog;
    private Button audioCallBtn;
    private Button videoCallBtn;
    private QBUser qbUser;
    private VideoChatConfig videoChatConfig;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_layout);
        initViews();
    }

    @Override
    public void onResume() {
        try {
            QBVideoChatController.getInstance().setQBVideoChatListener(UserHelper.getCurrentUser(), qbVideoChatListener);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    private void initViews() {
        int userId = getIntent().getIntExtra("userId", 0);
        qbUser = new QBUser(userId);

        audioCallBtn = (Button) findViewById(R.id.audioCallBtn);
        videoCallBtn = (Button) findViewById(R.id.videoCallBtn);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));

        videoCallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progressDialog != null && !progressDialog.isShowing()) {
                    progressDialog.show();
                }
                videoChatConfig = QBVideoChatController.getInstance(). callFriend(qbUser, CallType.VIDEO_AUDIO, null);
            }
        });

        audioCallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (progressDialog != null && !progressDialog.isShowing()) {
                    progressDialog.show();
                }
                videoChatConfig = QBVideoChatController.getInstance().callFriend(qbUser, CallType.AUDIO, null);
            }
        });
    }

    private void showCallDialog() {
        autoCancelHandler.postDelayed(autoCancelTask, 30000);
        alertDialog = DialogHelper.showCallDialog(this, new OnCallDialogListener() {
            @Override
            public void onAcceptCallClick() {
                QBVideoChatController.getInstance().acceptCallByFriend(videoChatConfig, null);
                startVideoChatActivity();
                autoCancelHandler.removeCallbacks(autoCancelTask);
            }

            @Override
            public void onRejectCallClick() {
                QBVideoChatController.getInstance().rejectCall(videoChatConfig);
                autoCancelHandler.removeCallbacks(autoCancelTask);
            }
        }, new Dialog.OnCancelListener(){

            @Override
            public void onCancel(DialogInterface dialog) {
                QBVideoChatController.getInstance().rejectCall(videoChatConfig);
                autoCancelHandler.removeCallbacks(autoCancelTask);
            }
        });
    }

    private void startVideoChatActivity() {
        Intent intent = new Intent(getBaseContext(), ActivityVideoChat.class);
        intent.putExtra(VideoChatConfig.class.getCanonicalName(), videoChatConfig);
        startActivity(intent);
    }

    private OnQBVideoChatListener qbVideoChatListener = new OnQBVideoChatListener() {
        @Override
        public void onVideoChatStateChange(CallState state, VideoChatConfig receivedVideoChatConfig) {
            Debugger.logConnection("onVideoChatStateChange: " + state);
            videoChatConfig = receivedVideoChatConfig;
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            switch (state) {
                case ACCEPT:
                    showCallDialog();
                    break;
                case ON_ACCEPT_BY_USER:
                    QBVideoChatController.getInstance().onAcceptFriendCall(videoChatConfig, null);
                    startVideoChatActivity();
                    break;
                case ON_REJECTED_BY_USER:
                    Toast.makeText(ActivityCallUser.this, "Rejected by user", Toast.LENGTH_SHORT).show();
                    break;
                case ON_DID_NOT_ANSWERED:
                    Toast.makeText(ActivityCallUser.this, "User did not answer", Toast.LENGTH_SHORT).show();
                    break;
                case ON_CANCELED_CALL:
                    videoChatConfig = null;
                    if (alertDialog != null && alertDialog.isShowing()){
                        alertDialog.dismiss();
                    }
                    autoCancelHandler.removeCallbacks(autoCancelTask);
                    break;
            }
        }
    };

    private Runnable autoCancelTask = new Runnable() {
        @Override
        public void run() {
            if (alertDialog != null && alertDialog.isShowing()){
                alertDialog.dismiss();
            }
        }
    };
}