package com.joyfe.videochat.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.joyfe.videochat.R;
import com.joyfe.videochat.async.QBCreateSessionCallback;
import com.joyfe.videochat.model.utils.UserHelper;
import com.quickblox.core.QBCallback;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.core.result.Result;
import com.quickblox.module.auth.QBAuth;
import com.quickblox.module.users.model.QBUser;

public class ActivityLogin extends Activity {

    private QBUser mUser;

    public ActivityLogin(){
        UserHelper.init(ActivityLogin.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        mUser = new QBUser(UserHelper.getUsername(), "password");
        QBSettings.getInstance().fastConfigInit("8972", "uTy4NE4scKR8zcQ", "6vLMt6Mj8jPTpJP");
        authorize(mUser, new QBCallbackImpl(){
            @Override
            public void onComplete(Result result) {
                startVideoChatActivity();
            }
        });
    }

    private void startVideoChatActivity() {
        Intent intent = new Intent(this, ActivityRandomChat.class);
        startActivity(intent);
        finish();
    }

    private void showCallUserActivity(QBUser user) {
        Intent intent = new Intent(this, ActivityCallUser.class);
        //TODO:
        intent.putExtra("userId", mUser.getId().equals(996294) ? 996291 : 996294);
        startActivity(intent);
        finish();
    }


    private void authorize(QBUser user, QBCallback callback) {
        QBAuth.createSession(user.getLogin(), user.getPassword(), new QBCreateSessionCallback(user, callback));
    }
}