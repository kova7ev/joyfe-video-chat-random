package com.joyfe.videochat.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.joyfe.videochat.R;
import com.joyfe.videochat.async.GetUsersAsyncTask;
import com.joyfe.videochat.async.SendLivePacketAsyncTask;
import com.joyfe.videochat.async.UITimerTask;
import com.joyfe.videochat.model.Consts;
import com.joyfe.videochat.model.PlayButtonState;
import com.joyfe.videochat.model.UserState;
import com.joyfe.videochat.model.listener.OnCallDialogListener;
import com.joyfe.videochat.model.utils.CustomObjectHelper;
import com.joyfe.videochat.model.utils.DialogHelper;
import com.joyfe.videochat.model.utils.UserHelper;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.module.chat.QBChatService;
import com.quickblox.module.custom.model.QBCustomObject;
import com.quickblox.module.custom.result.QBCustomObjectLimitedResult;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.users.result.QBUserPagedResult;
import com.quickblox.module.videochat.core.QBVideoChatController;
import com.quickblox.module.videochat.model.listeners.OnCameraViewListener;
import com.quickblox.module.videochat.model.listeners.OnQBVideoChatListener;
import com.quickblox.module.videochat.model.objects.CallState;
import com.quickblox.module.videochat.model.objects.CallType;
import com.quickblox.module.videochat.model.objects.VideoChatConfig;
import com.quickblox.module.videochat.views.CameraView;

import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import jp.co.cyberagent.android.gpuimage.OpponentGlSurfaceView;

public class ActivityRandomChat extends Activity {

    private CameraView cameraView;
    private OpponentGlSurfaceView opponentView;
    private ProgressBar opponentImageLoadingPb;
    private TextView onlineUsers;
    private ImageButton playButton;
    private ImageButton nextButton;
    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;

    private VideoChatConfig videoChatConfig;
    private Handler autoCancelHandler = new Handler(Looper.getMainLooper());
    private PlayButtonState playButtonState = PlayButtonState.Stop;

    private ArrayList<QBUser> ignoredUsers = new ArrayList<QBUser>();
    private ArrayList<QBUser> allOnlineUsers = new ArrayList<QBUser>();
    private ArrayList<QBUser> users = new ArrayList<QBUser>();

    private QBUser lastMemorizedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.random_chat_layout);

        initViews();

        videoChatConfig = getIntent().getParcelableExtra(VideoChatConfig.class.getCanonicalName());
        try {
            QBVideoChatController.getInstance().setQBVideoChatListener(UserHelper.getCurrentUser(), qbVideoChatListener);
            QBChatService.getInstance().startAutoSendPresence(3);
        } catch (XMPPException e) {
            e.printStackTrace();
        }

        checkUsersOnlineAsync();
        sendLivePacketAsync();
    }

    private void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));

        opponentImageLoadingPb = (ProgressBar) findViewById(R.id.opponentImageLoading);

        opponentView = (OpponentGlSurfaceView) findViewById(R.id.opponentView);

        onlineUsers = (TextView) findViewById(R.id.onlineUsers);

        cameraView = (CameraView) findViewById(R.id.cameraView);
        cameraView.setQBVideoChatListener(qbVideoChatListener);
        cameraView.setFPS(6);
        cameraView.setCameraFrameProcess(true);
        cameraView.setZOrderOnTop(true);

        playButton = (ImageButton) findViewById(R.id.play_pause_button);
        playButton.setEnabled(false);
        playButton.setOnClickListener(playButtonOnClickListener);

        nextButton = (ImageButton) findViewById(R.id.next_button);
        nextButton.setEnabled(false);
        nextButton.setOnClickListener(nextButtonOnClickListener);

        cameraView.setOnCameraViewListener(new OnCameraViewListener() {
            @Override
            public void onCameraSupportedPreviewSizes(List<Camera.Size> supportedPreviewSizes) {
                Camera.Size firstFrameSize = supportedPreviewSizes.get(0);
                Camera.Size lastFrameSize = supportedPreviewSizes.get(supportedPreviewSizes.size() - 1);
                cameraView.setFrameSize(firstFrameSize.width > lastFrameSize.width ? lastFrameSize : firstFrameSize);
            }
        });

        videoChatConfig = getIntent().getParcelableExtra(VideoChatConfig.class.getCanonicalName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.reuseCameraView();
    }

    @Override
    protected void onPause() {
        cameraView.closeCamera();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        QBVideoChatController.getInstance().finishVideoChat(videoChatConfig);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
    }

    private void showCallDialog() {
        autoCancelHandler.postDelayed(autoCancelTask, 30000);
        alertDialog = DialogHelper.showCallDialog(this, new OnCallDialogListener() {
            @Override
            public void onAcceptCallClick() {
                QBVideoChatController.getInstance().acceptCallByFriend(videoChatConfig, null);
                //startVideoChatActivity();
                autoCancelHandler.removeCallbacks(autoCancelTask);
            }

            @Override
            public void onRejectCallClick() {
                playButton.post(new Runnable() {
                    @Override
                    public void run() {
                        playButton.setEnabled(true);
                        playButton.setImageResource(R.drawable.play_button);
                    }
                });

                nextButton.post(new Runnable() {
                    @Override
                    public void run() {
                        nextButton.setImageResource(R.drawable.next_button);
                        nextButton.setEnabled(true);
                    }
                });

                playButtonState = PlayButtonState.Pause;

                QBVideoChatController.getInstance().rejectCall(videoChatConfig);
                autoCancelHandler.removeCallbacks(autoCancelTask);
            }
        }, new Dialog.OnCancelListener(){

            @Override
            public void onCancel(DialogInterface dialog) {
                QBVideoChatController.getInstance().rejectCall(videoChatConfig);
                autoCancelHandler.removeCallbacks(autoCancelTask);
            }
        });
    }

    private void checkUsersOnlineAsync() {
        Timer timer = new Timer();
        UITimerTask timerTask = new UITimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        GetUsersAsyncTask task = new GetUsersAsyncTask();
                        QBCallbackImpl impl = new QBCallbackImpl() {

                            @Override
                            public void onComplete(com.quickblox.core.result.Result result) {
                                final ArrayList<QBUser> allUsers = ((QBUserPagedResult) result).getUsers();
                                final ArrayList<QBUser> onlyNewUsers = new ArrayList<QBUser>();
                                final ArrayList<QBUser> onlyOnlineUsers = new ArrayList<QBUser>();

                                CustomObjectHelper.getCustomObjects(new QBCallbackImpl() {
                                    @Override
                                    public void onComplete(com.quickblox.core.result.Result result) {

                                        List<QBCustomObject> qbCustomObjects = ((QBCustomObjectLimitedResult) result).getCustomObjects();

                                        for (int i = 0; i < allUsers.size(); i++) {
                                            QBUser u = allUsers.get(i);
                                            QBCustomObject obj = getCustomObjectById(qbCustomObjects, u.getId());

                                            if (obj != null) {
                                                UserState userState = new UserState(obj);

                                                if (UserHelper.isOnline(u, userState) && !UserHelper.isCurrentUser(u)) {
                                                    onlyOnlineUsers.add(u);
                                                }

                                                if (UserHelper.isOnline(u, userState) && !UserHelper.isCurrentUser(u) && !UserHelper.isIgnored(u, ignoredUsers)) {
                                                    onlyNewUsers.add(u);
                                                }
                                            }
                                        }
                                        allOnlineUsers = onlyOnlineUsers;
                                        users = onlyNewUsers;

                                        if (playButtonState == PlayButtonState.Stop) {
                                            playButton.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    playButton.setEnabled(users.size() > 0);
                                                    playButton.setImageResource(users.size() > 0 ? R.drawable.play_button : R.drawable.play_button_disabled);
                                                }
                                            });

                                            nextButton.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    nextButton.setEnabled(users.size() > 0);
                                                    nextButton.setImageResource(users.size() > 0 ? R.drawable.next_button : R.drawable.next_button_disabled);
                                                }
                                            });
                                        }

                                        onlineUsers.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                onlineUsers.setText(String.format("%d online", allOnlineUsers.size()));
                                            }
                                        });
                                    }
                                });
                            }
                        };
                        task.execute(impl);
                    }
                });
            }
        };
        timer.schedule(timerTask, 0, 3000);
    }

    private void sendLivePacketAsync() {
        Timer timer = new Timer();
        UITimerTask timerTask = new UITimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        SendLivePacketAsyncTask task = new SendLivePacketAsyncTask(UserHelper.getCurrentUser());
                        task.execute(new QBCallbackImpl());
                    }
                });
            }
        };
        timer.schedule(timerTask, 0, 3000);
    }

    private QBCustomObject getCustomObjectById(List<QBCustomObject> objects, int userId) {
        for (QBCustomObject obj : objects) {
            int id = Integer.parseInt(obj.getFields().get(Consts.USER_ID).toString());
            if (id == userId) {
                return obj;
            }
        }

        return null;
    }

    private Runnable autoCancelTask = new Runnable() {
        @Override
        public void run() {
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
        }
    };

    private OnQBVideoChatListener qbVideoChatListener = new OnQBVideoChatListener() {

        @Override
        public void onVideoChatStateChange(CallState state, VideoChatConfig receivedVideoChatConfig) {
            videoChatConfig = receivedVideoChatConfig;

            switch (state) {
                case ACCEPT:
                    QBUser user = new QBUser(receivedVideoChatConfig.getOpponentId());
                    ignoredUsers.add(user);
                    rememberUser(user);
                    QBVideoChatController.getInstance().finishActiveVideoChat();
                    showCallDialog();
                    break;
                case ON_ACCEPT_BY_USER:
                    QBVideoChatController.getInstance().onAcceptFriendCall(videoChatConfig, null);

                    opponentView.post(new Runnable() {
                        @Override
                        public void run() {
                            opponentView.setVisibility(View.VISIBLE);
                        }
                    });

                    nextButton.post(new Runnable() {
                        @Override
                        public void run() {
                            nextButton.setImageResource(R.drawable.next_button);
                            nextButton.setEnabled(true);
                        }
                    });

                    playButton.post(new Runnable() {
                        @Override
                        public void run() {
                            playButton.setImageResource(R.drawable.pause_button);
                        }
                    });

                    //startVideoChatActivity();
                    break;
                case ON_REJECTED_BY_USER:
                    Toast.makeText(ActivityRandomChat.this, "Rejected by user", Toast.LENGTH_SHORT).show();

                    opponentView.post(new Runnable() {
                        @Override
                        public void run() {
                            opponentView.setVisibility(View.INVISIBLE);
                        }
                    });

                    QBVideoChatController.getInstance().finishActiveVideoChat();

                    playButton.post(new Runnable() {
                        @Override
                        public void run() {
                            playButton.setImageResource(R.drawable.play_button);
                        }
                    });
                    playButtonState = PlayButtonState.Pause;
                    break;
                case ON_DID_NOT_ANSWERED:
                    Toast.makeText(ActivityRandomChat.this, "User did not answer", Toast.LENGTH_SHORT).show();
                    opponentView.post(new Runnable() {
                        @Override
                        public void run() {
                            opponentView.setVisibility(View.INVISIBLE);
                        }
                    });

                    QBVideoChatController.getInstance().finishActiveVideoChat();

                    playButton.post(new Runnable() {
                        @Override
                        public void run() {
                            playButton.setImageResource(R.drawable.play_button);
                        }
                    });
                    playButtonState = PlayButtonState.Pause;
                    break;
                case ON_CANCELED_CALL:
                    videoChatConfig = null;
                    if (alertDialog != null && alertDialog.isShowing()) {
                        alertDialog.dismiss();
                    }
                    autoCancelHandler.removeCallbacks(autoCancelTask);
                    playButtonState = PlayButtonState.Pause;
                    opponentView.post(new Runnable() {
                        @Override
                        public void run() {
                            opponentView.setVisibility(View.INVISIBLE);
                        }
                    });
                    break;
                case ON_CALL_END:
                    playButtonState = PlayButtonState.Pause;
                    opponentView.post(new Runnable() {
                        @Override
                        public void run() {
                            opponentView.setVisibility(View.INVISIBLE);
                        }
                    });
                    playButton.post(new Runnable() {
                        @Override
                        public void run() {
                            playButton.setImageResource(R.drawable.play_button);
                        }
                    });
                    break;
            }
        }

        @Override
        public void onCameraDataReceive(byte[] videoData) {
            if (videoChatConfig != null && videoChatConfig.getCallType() != CallType.VIDEO_AUDIO) {
                return;
            }
            QBVideoChatController.getInstance().sendVideo(videoData);
        }

        @Override
        public void onMicrophoneDataReceive(byte[] audioData) {
            QBVideoChatController.getInstance().sendAudio(audioData);
        }

        @Override
        public void onOpponentVideoDataReceive(byte[] videoData) {
            opponentView.loadOpponentImage(videoData);
        }

        @Override
        public void onOpponentAudioDataReceive(byte[] audioData) {
            QBVideoChatController.getInstance().playAudio(audioData);
        }

        @Override
        public void onProgress(boolean progress) {
            if (progress) {
                playButton.post(new Runnable() {
                    @Override
                    public void run() {
                        playButton.setEnabled(true);
                        playButton.setImageResource(R.drawable.pause_button);
                    }
                });

                nextButton.post(new Runnable() {
                    @Override
                    public void run() {
                        nextButton.setImageResource(R.drawable.next_button);
                        nextButton.setEnabled(true);
                    }
                });

                playButtonState = PlayButtonState.Play;

                opponentView.post(new Runnable() {
                    @Override
                    public void run() {
                        opponentView.setVisibility(View.VISIBLE);
                    }
                });
            }
            opponentImageLoadingPb.setVisibility(progress ? View.VISIBLE : View.GONE);
        }
    };

    private View.OnClickListener playButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (playButtonState == PlayButtonState.Stop) {
                QBUser user = getNextAvailableUser();
                rememberUser(user);

                videoChatConfig = QBVideoChatController.getInstance().callFriend(user, CallType.VIDEO_AUDIO, null);
                playButtonState = PlayButtonState.Play;

            } else if (playButtonState == PlayButtonState.Play) {
                opponentView.post(new Runnable() {
                    @Override
                    public void run() {
                        opponentView.setVisibility(View.INVISIBLE);
                    }
                });

                QBVideoChatController.getInstance().finishActiveVideoChat();

                playButton.post(new Runnable() {
                    @Override
                    public void run() {
                        playButton.setImageResource(R.drawable.play_button);
                    }
                });
                playButtonState = PlayButtonState.Pause;

            } else if (playButtonState == PlayButtonState.Pause) {
                videoChatConfig = QBVideoChatController.getInstance().callFriend(getMemorizedUser(), CallType.VIDEO_AUDIO, null);
                /**
                opponentView.post(new Runnable() {
                    @Override
                    public void run() {
                        opponentView.setVisibility(View.VISIBLE);
                    }
                });

                playButton.post(new Runnable() {
                    @Override
                    public void run() {
                        playButton.setImageResource(R.drawable.pause_button);
                    }
                });*/
                playButtonState = PlayButtonState.Play;
            }
        }
    };

    private View.OnClickListener nextButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (users.size() == 0) {
                alertDialog = DialogHelper.showNextUserDialog(ActivityRandomChat.this, new OnCallDialogListener() {
                    @Override
                    public void onAcceptCallClick() {
                        users = allOnlineUsers;
                        ignoredUsers.clear();

                        QBUser user = getNextAvailableUser();
                        rememberUser(user);

                        videoChatConfig = QBVideoChatController.getInstance().callFriend(user, CallType.VIDEO_AUDIO, null);
                        /**
                        opponentView.post(new Runnable() {
                            @Override
                            public void run() {
                                opponentView.setVisibility(View.VISIBLE);
                            }
                        });*/
                    }

                    @Override
                    public void onRejectCallClick() {
                    }
                }, new Dialog.OnCancelListener(){

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
            } else {

                QBUser user = getNextAvailableUser();
                rememberUser(user);

                videoChatConfig = QBVideoChatController.getInstance().callFriend(user, CallType.VIDEO_AUDIO, null);
                /**
                opponentView.post(new Runnable() {
                    @Override
                    public void run() {
                        opponentView.setVisibility(View.VISIBLE);
                    }
                });*/
            }
        }
    };

    private QBUser getNextAvailableUser() {
        if (users == null || users.size() == 0) {
            return null;
        }

        int index = (int) (Math.random() * users.size() - 1);
        QBUser user = users.remove(index);
        ignoredUsers.add(user);

        return user;
    }

    private void rememberUser(QBUser user) {
        lastMemorizedUser = user;
    }

    private QBUser getMemorizedUser() {
        return lastMemorizedUser;
    }
}