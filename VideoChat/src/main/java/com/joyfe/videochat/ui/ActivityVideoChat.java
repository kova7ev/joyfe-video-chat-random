package com.joyfe.videochat.ui;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.joyfe.videochat.R;
import com.joyfe.videochat.async.GetUsersAsyncTask;
import com.joyfe.videochat.async.UITimerTask;
import com.joyfe.videochat.model.utils.CustomObjectHelper;
import com.joyfe.videochat.model.utils.UserHelper;
import com.quickblox.core.QBCallbackImpl;
import com.quickblox.module.chat.QBChatService;
import com.quickblox.module.custom.model.QBCustomObject;
import com.quickblox.module.custom.result.QBCustomObjectLimitedResult;
import com.quickblox.module.users.model.QBUser;
import com.quickblox.module.users.result.QBUserPagedResult;
import com.quickblox.module.videochat.core.QBVideoChatController;
import com.quickblox.module.videochat.model.listeners.OnCameraViewListener;
import com.quickblox.module.videochat.model.listeners.OnQBVideoChatListener;
import com.quickblox.module.videochat.model.objects.CallState;
import com.quickblox.module.videochat.model.objects.CallType;
import com.quickblox.module.videochat.model.objects.VideoChatConfig;
import com.quickblox.module.videochat.views.CameraView;

import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import jp.co.cyberagent.android.gpuimage.OpponentGlSurfaceView;

public class ActivityVideoChat extends Activity {

    private CameraView cameraView;
    private OpponentGlSurfaceView opponentView;
    private ProgressBar opponentImageLoadingPb;
    private VideoChatConfig videoChatConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_chat_layout);
        initViews();

        videoChatConfig = getIntent().getParcelableExtra(VideoChatConfig.class.getCanonicalName());
        try {
            QBVideoChatController.getInstance().setQBVideoChatListener(UserHelper.getCurrentUser(), qbVideoChatListener);
            QBChatService.getInstance().startAutoSendPresence(3);
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    private void initViews() {
        opponentView = (OpponentGlSurfaceView) findViewById(R.id.opponentView);
        opponentImageLoadingPb = (ProgressBar) findViewById(R.id.opponentImageLoading);

        cameraView = (CameraView) findViewById(R.id.cameraView);
        cameraView.setCameraFrameProcess(true);
        cameraView.setQBVideoChatListener(qbVideoChatListener);
        cameraView.setFPS(6);
        cameraView.setOnCameraViewListener(new OnCameraViewListener() {
            @Override
            public void onCameraSupportedPreviewSizes(List<Camera.Size> supportedPreviewSizes) {
                Camera.Size firstFrameSize = supportedPreviewSizes.get(0);
                Camera.Size lastFrameSize = supportedPreviewSizes.get(supportedPreviewSizes.size() - 1);
                cameraView.setFrameSize(firstFrameSize.width > lastFrameSize.width ? lastFrameSize : firstFrameSize);
            }
        });

        videoChatConfig = getIntent().getParcelableExtra(VideoChatConfig.class.getCanonicalName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.reuseCameraView();
    }

    @Override
    protected void onPause() {
        cameraView.closeCamera();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        QBVideoChatController.getInstance().finishVideoChat(videoChatConfig);
        super.onDestroy();
    }

    OnQBVideoChatListener qbVideoChatListener = new OnQBVideoChatListener() {
        @Override
        public void onCameraDataReceive(byte[] videoData) {
            if (videoChatConfig.getCallType() != CallType.VIDEO_AUDIO) {
                return;
            }
            QBVideoChatController.getInstance().sendVideo(videoData);
        }

        @Override
        public void onMicrophoneDataReceive(byte[] audioData) {
            QBVideoChatController.getInstance().sendAudio(audioData);
        }

        @Override
        public void onOpponentVideoDataReceive(byte[] videoData) {
            opponentView.loadOpponentImage(videoData);
        }

        @Override
        public void onOpponentAudioDataReceive(byte[] audioData) {
            QBVideoChatController.getInstance().playAudio(audioData);
        }

        @Override
        public void onProgress(boolean progress) {
            opponentImageLoadingPb.setVisibility(progress ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onVideoChatStateChange(CallState callState, VideoChatConfig chat) {
            switch (callState) {
                case ON_CALL_START:
                    Toast.makeText(getBaseContext(), getString(R.string.call_start_txt), Toast.LENGTH_SHORT).show();
                    break;
                case ON_CANCELED_CALL:
                    Toast.makeText(getBaseContext(), getString(R.string.call_canceled_txt), Toast.LENGTH_SHORT).show();
                    finish();
                    break;
                case ON_CALL_END:
                    finish();
                    break;
            }
        }
    };
}